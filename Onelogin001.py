#!/usr/bin/python
import requests
import re
import json

def crewsfind(shipd):
    print ('\x1b[0;35;40m' + 'Result for Exercise 3:\t','-- >  Find all ships that with crews between 3 and 100\n' + '\x1b[0m')
    val = re.split(',', shipd['crew'][:3])
    try :
        res = map(int, val)
        for x in res:
            if (x >= 3) and (x <= 100):
                print ('\x1b[0;35;40m' + str(shipd['crew']) +'\x1b[0m')
            else:
                print ('\x1b[1;35;40m' + 'is out of Range  3 and 100\t Sorry!' + '\x1b[0m')
                
    except Exception as e:
        print('\x1b[1;31;40m'+'Error -->', str(e) +'\x1b[0m')
        pass
                                
def hyperFind(ship):
    print ('Result for Exercise 2:\t','-- > Finding all ships that have a hyperdrive rating >= 1.0\n')
    i = requests.get(ship)
    shipd = json.loads(i.content)
    if (float(shipd['hyperdrive_rating']) >= 1.0):
        print('Result for Exercise 2:\t', shipd)
    else:
        print ('\x1b[1;31;40m' + 'hyperdrive_rating is lees than 1.0\t Sorry!' + '\x1b[0m')

    crewsfind(shipd)
   
def allInfo():
    r = requests.get('https://swapi.dev/api/films/3/')
    if r.status_code is not 200:
        print("Got a " + str(r.status_code) + " so stopping")
    else:
        data = json.loads(r.content)
        print ('\x1b[0;33;40m' +'Exercise 1:\t' + "All ships that appeared in Return of the Jedi Are conted by the Index:\n" +'\x1b[0m')
        for i, ship in enumerate(data['starships'][:], start=1) :
            print ('\x1b[0;33;40m'+'Result for \t' +"Exercise 1:\n" +'\x1b[0m')
            print (i,'\x1b[0;33;40m'+ ship +'\x1b[0m')
            hyperFind(ship)

allInfo()
