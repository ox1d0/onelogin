# onelogin Scripting ability exercise:
Using the Star Wars API, available at https://swapi.dev/, write a client:

Exercise 1:  Find all ships that appeared in Return of the Jedi

Exercise 2:  Find all ships that have a hyperdrive rating >= 1.0.

Exercise 3:  Find all ships that have crews between 3 and 100.


--------------------SOLUTION-------------------------------------------------------------------

I glad to know that python is a standard language that the OneLogin DevOps team use for automation. 
XD- because is my Favorite language  too

I've written 3 Functions:

1- crewsfind:
it will check for Exercise 3. then it will check for exceptions on data in case.

2- hyperFind:
it will check for Exercise 2. then it will send the shipd object to crewsfind.

3- allInfo 
it will check for Exercise 1. then it will send the  ship object to hyperFind.

Using re lib I'm retriaving the information on allInfo(), then estoring the content on data var wich
will olds the dict for all of the ships that appeared in Return of the Jedi movie.

then it will send the ship object by hyperFind(ship) that will request for every URL in order to retriaving  all of the data for every ship on the target movie dict, then it will test for 'hyperdrive_rating'  >= 1.0.

it will follow on the main cycle ( enumerate(data['starships'][:]) ) so it will print out only if the object have passed the test else it will show a red message as a warning for the not matching condition and it will continue by sending shipd object ( contains the json information for each ship on the dict) to the crewsfind(shipd) function.

crewsfind will handler some exceptions because information retrieved has not a clear delimiter caracter.
then it will check for the conditions crews between 3 and 100.

It will print a result only if the condition is met else it will print a Sorry! message.

at this point all of the filters shold be tested ending with the cycle.


Note:
The output is some like this (every iteration):
_______________ 
index   result
_______________
7       http://swapi.dev/api/starships/17/
Result for 	Exercise 2:	 -- > Finding all ships that have a hyperdrive rating >= 1.0

{'name': 'Rebel transport', 'model': 'GR-75 medium transport', 'manufacturer': 'Gallofree Yards, Inc.', 'cost_in_credits': 'unknown', 'length': '90', 'max_atmosphering_speed': '650', 'crew': '6', 'passengers': '90', 'cargo_capacity': '19000000', 'consumables': '6 months', 'hyperdrive_rating': '4.0', 'MGLT': '20', 'starship_class': 'Medium transport', 'pilots': [], 'films': ['http://swapi.dev/api/films/2/', 'http://swapi.dev/api/films/3/'], 'created': '2014-12-15T12:34:52.264000Z', 'edited': '2014-12-20T21:23:49.895000Z', 'url': 'http://swapi.dev/api/starships/17/'} 

Exercise 3:	 -- >  Find all ships that with crews between 3 and 100

6

------------


Execute like any other python Script:

python Onelogin001.py


